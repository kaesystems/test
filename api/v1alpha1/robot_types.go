package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type ROS struct {
	Version string `json:"version,omitempty"`
	Distro  string `json:"distro,omitempty"`
}

type RobotSpec struct {
	ROS ROS `json:"ros,omitempty"`
}

type WorkspaceSetup struct {
	Phase string `json:"phase,omitempty"`
}

type Storage struct {
	Ready              bool           `json:"ready,omitempty"`
	WorkspaceSetup     WorkspaceSetup `json:"workspaceSetup,omitempty"`
	WorkspaceMountPath string         `json:"workspaceMountPath,omitempty"`
}

type PodStatus struct {
	AdapterConfig bool   `json:"adapterConfig,omitempty"`
	PodPhase      string `json:"podPhase,omitempty"`
}

type Service struct {
	Name string `json:"name,omitempty"`
	Port int    `json:"port,omitempty"`
}

type ServiceConfig struct {
	IsAvailable bool      `json:"isAvailable,omitempty"`
	Services    []Service `json:"services,omitempty"`
}

type RobotStatus struct {
	Storage       Storage       `json:"storage,omitempty"`
	PodStatus     PodStatus     `json:"podStatus,omitempty"`
	ServiceConfig ServiceConfig `json:"serviceConfig,omitempty"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

type Robot struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   RobotSpec   `json:"spec,omitempty"`
	Status RobotStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

type RobotList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Robot `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Robot{}, &RobotList{})
}
