package spawn

import (
	robotv1alpha1 "github.com/roboscale/robot/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
)

func GetPVC(cr *robotv1alpha1.Robot, pvcNamespacedName *types.NamespacedName) (*corev1.PersistentVolumeClaim, error) {
	storageClassName := "openebs-hostpath"

	pvc := &corev1.PersistentVolumeClaim{
		ObjectMeta: metav1.ObjectMeta{
			Name:      pvcNamespacedName.Name,
			Namespace: pvcNamespacedName.Namespace,
		},
		Spec: corev1.PersistentVolumeClaimSpec{
			StorageClassName: &storageClassName,
			AccessModes: []corev1.PersistentVolumeAccessMode{
				corev1.ReadWriteOnce,
			},
			Resources: corev1.ResourceRequirements{
				Requests: corev1.ResourceList{
					corev1.ResourceName(corev1.ResourceStorage): resource.MustParse("1G"),
				},
			},
		},
	}

	return pvc, nil
}

// kind: PersistentVolumeClaim
// apiVersion: v1
// metadata:
//   name: mypvc
// spec:
//   storageClassName: openebs-hostpath
//   accessModes:
//     - ReadWriteOnce
//   resources:
//     requests:
//       storage: 1G
