package spawn

import (
	robotv1alpha1 "github.com/roboscale/robot/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/apimachinery/pkg/util/intstr"
)

func GetService(cr *robotv1alpha1.Robot, serviceNamespacedName *types.NamespacedName) (*corev1.Service, error) {

	service := &corev1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:      serviceNamespacedName.Name,
			Namespace: serviceNamespacedName.Namespace,
		},
		Spec: corev1.ServiceSpec{
			Type: corev1.ServiceTypeNodePort,
			Selector: map[string]string{
				"tuna": "app",
			},
			Ports: []corev1.ServicePort{
				{
					Name: "ros-pebble",
					Port: 7070,
					TargetPort: intstr.IntOrString{
						Type:   intstr.Int,
						IntVal: 7070,
					},
				},
				{
					Name: "ros-tracker",
					Port: 5000,
					TargetPort: intstr.IntOrString{
						Type:   intstr.Int,
						IntVal: 5000,
					},
				},
				{
					Name: "adapter-pebble",
					Port: 6060,
					TargetPort: intstr.IntOrString{
						Type:   intstr.Int,
						IntVal: 6060,
					},
				},
			},
		},
	}

	return service, nil

}
