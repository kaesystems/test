package spawn

import (
	"fmt"

	robotv1alpha1 "github.com/roboscale/robot/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
)

func GetAdapterConfigMap(cr *robotv1alpha1.Robot, cmNamespacedName *types.NamespacedName) (*corev1.ConfigMap, error) {

	configMap := &corev1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Name:      cmNamespacedName.Name,
			Namespace: cmNamespacedName.Namespace,
		},
		Data: map[string]string{
			"adapter": "config",
		},
	}

	fmt.Printf("%+v\n", *configMap)

	return configMap, nil
}
