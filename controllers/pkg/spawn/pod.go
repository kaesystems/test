package spawn

import (
	robotv1alpha1 "github.com/roboscale/robot/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
)

func GetPod(cr *robotv1alpha1.Robot, podNamespacedName *types.NamespacedName, configMapNamespacedName *types.NamespacedName) (*corev1.Pod, error) {

	rosPebblePort := corev1.ContainerPort{
		Name:          "ros-pebble",
		ContainerPort: 7070,
	}

	rosTrackerPort := corev1.ContainerPort{
		Name:          "ros-tracker",
		ContainerPort: 5000,
	}

	adapterPebblePort := corev1.ContainerPort{
		Name:          "adapter-pebble",
		ContainerPort: 6060,
	}

	shareProcessNamespace := true

	rosImage := "tunahanertekin/noetic-pebble:v0.0.7"
	adapterImage := "tunahanertekin/adapter:v0.3.1"
	trackerImage := "tunahanertekin/tracker-pebble:v0.1.0"

	rosContainerName := "ros"
	rosContainerType := "main"
	rosContainer := corev1.Container{
		Name:    rosContainerName,
		Image:   rosImage,
		Command: []string{"/bin/bash", "-c", "pebble run"},
		VolumeMounts: []corev1.VolumeMount{
			GetAdapterVolumeMount(rosContainerType, rosContainerName, configMapNamespacedName.Name),
		},
		Ports: []corev1.ContainerPort{
			rosPebblePort, rosTrackerPort,
		},
	}

	adapterContainerName := "adapter"
	adapterContainer := corev1.Container{
		Name:    adapterContainerName,
		Image:   adapterImage,
		Command: []string{"/bin/bash", "-c", "pebble run"},
		Ports: []corev1.ContainerPort{
			adapterPebblePort,
		},
	}

	trackerContainerName := "tracker"
	trackerContainerType := "sidecar"
	trackerContainer := corev1.Container{
		Name:    trackerContainerName,
		Image:   trackerImage,
		Command: []string{"/bin/bash", "-c", "sleep infinity"},
		VolumeMounts: []corev1.VolumeMount{
			GetAdapterVolumeMount(trackerContainerType, trackerContainerName, configMapNamespacedName.Name),
		},
	}

	ubuntuContainerName := "ubuntu"
	ubuntuContainer := corev1.Container{
		Name:    ubuntuContainerName,
		Image:   "ubuntu:focal",
		Command: []string{"sleep", "infinity"},
	}

	pod := &corev1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			Name:      podNamespacedName.Name,
			Namespace: podNamespacedName.Namespace,
			Labels: map[string]string{
				"tuna": "app",
			},
		},
		Spec: corev1.PodSpec{
			ShareProcessNamespace: &shareProcessNamespace,
			Containers: []corev1.Container{
				rosContainer, adapterContainer, trackerContainer, ubuntuContainer,
			},
			Volumes: []corev1.Volume{
				GetAdapterVolume(*configMapNamespacedName),
			},
			RestartPolicy: corev1.RestartPolicyNever,
		},
	}

	return pod, nil

}

func GetAdapterVolume(configMapNamespacedName types.NamespacedName) corev1.Volume {

	volume := corev1.Volume{
		Name: configMapNamespacedName.Name,
		VolumeSource: corev1.VolumeSource{
			ConfigMap: &corev1.ConfigMapVolumeSource{
				LocalObjectReference: corev1.LocalObjectReference{
					Name: configMapNamespacedName.Name,
				},
				Items: []corev1.KeyToPath{
					{
						Key:  "adapter",
						Path: "keys",
					},
				},
			},
		},
	}

	return volume
}

func GetAdapterVolumeMount(containerType string, containerName string, volumeName string) corev1.VolumeMount {

	mountPath := "/etc/container/"

	adapterVolumeMount := corev1.VolumeMount{
		Name:      volumeName,
		MountPath: mountPath + containerType + "_" + containerName,
	}

	return adapterVolumeMount

}

// volumes:
// - name: adapter-config
//   configMap:
// 	name: adapter
// 	items:
// 	- key: adapter
// 	  path: keys
