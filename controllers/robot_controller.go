package controllers

import (
	"context"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/rest"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"

	robotv1alpha1 "github.com/roboscale/robot/api/v1alpha1"
	"github.com/roboscale/robot/controllers/pkg/spawn"
)

// RobotReconciler reconciles a Robot object
type RobotReconciler struct {
	client.Client
	RESTClient rest.Interface
	RESTConfig *rest.Config
	Scheme     *runtime.Scheme
}

//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robots,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robots/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=robot.roboscale.io,resources=robots/finalizers,verbs=update

func (r *RobotReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	logger := log.FromContext(ctx)
	logger.Info("Reconciling robot...")

	// DEFINE OWNED RESOURCE METADATA

	pvcNamespacedName := types.NamespacedName{
		Namespace: req.Namespace,
		Name:      req.Name + "-storage",
	}

	adapterConfigMapNamespacedName := types.NamespacedName{
		Namespace: req.Namespace,
		Name:      req.Name + "-adapter-config",
	}

	podNamespacedName := types.NamespacedName{
		Namespace: req.Namespace,
		Name:      req.Name,
	}

	serviceNamespacedName := types.NamespacedName{
		Namespace: req.Namespace,
		Name:      req.Name,
	}

	// END

	instance := &robotv1alpha1.Robot{}
	err := r.Get(ctx, req.NamespacedName, instance)
	if err != nil {
		if errors.IsNotFound(err) {
			return ctrl.Result{}, nil
		}
		return ctrl.Result{}, err
	}

	// CHECK CR STATUS AND ACT
	switch instance.Status.Storage.Ready {
	case true:
		logger.Info("STATUS: PVC is ready.")

	case false:
		logger.Info("STATUS: PVC is not ready. Creating...")

		pvc, err := spawn.GetPVC(instance, &pvcNamespacedName)
		if err != nil {
			return ctrl.Result{}, err
		}

		err = ctrl.SetControllerReference(instance, pvc, r.Scheme)
		if err != nil {
			return ctrl.Result{}, err
		}

		err = r.Create(ctx, pvc)
		if err != nil {
			return ctrl.Result{}, err
		}

		logger.Info("STATUS: PVC is created.")
		instance.Status.Storage.Ready = true
	}

	switch instance.Status.PodStatus.AdapterConfig {
	case true:
		logger.Info("STATUS: Adapter CM is ready.")
		switch instance.Status.PodStatus.PodPhase {
		case "":
			logger.Info("STATUS: Pod is not ready. Creating...")

			pod, err := spawn.GetPod(instance, &podNamespacedName, &adapterConfigMapNamespacedName)
			if err != nil {
				return ctrl.Result{}, err
			}

			err = ctrl.SetControllerReference(instance, pod, r.Scheme)
			if err != nil {
				return ctrl.Result{}, err
			}

			err = r.Create(ctx, pod)
			if err != nil {
				return ctrl.Result{}, err
			}

			logger.Info("STATUS: Pod is created.")
			instance.Status.PodStatus.PodPhase = "Created"

		case "Running":
			logger.Info("STATUS: Pod is running.")

			switch instance.Status.ServiceConfig.IsAvailable {
			case true:
				logger.Info("STATUS: Services are available.")
			case false:
				logger.Info("STATUS: Services are not available. Creating...")

				service, err := spawn.GetService(instance, &serviceNamespacedName)
				if err != nil {
					return ctrl.Result{}, err
				}

				err = ctrl.SetControllerReference(instance, service, r.Scheme)
				if err != nil {
					return ctrl.Result{}, err
				}

				err = r.Create(ctx, service)
				if err != nil {
					return ctrl.Result{}, err
				}

				logger.Info("STATUS: Service is created.")
				for _, v := range service.Spec.Ports {
					instance.Status.ServiceConfig.Services = append(instance.Status.ServiceConfig.Services, robotv1alpha1.Service{
						Name: v.Name,
						Port: int(v.NodePort),
					})
				}
				instance.Status.ServiceConfig.IsAvailable = true

			}
		}

	case false:
		logger.Info("STATUS: Adapter CM is not ready. Creating...")

		adapterCM, err := spawn.GetAdapterConfigMap(instance, &adapterConfigMapNamespacedName)
		if err != nil {
			return ctrl.Result{}, err
		}

		err = ctrl.SetControllerReference(instance, adapterCM, r.Scheme)
		if err != nil {
			return ctrl.Result{}, err
		}

		err = r.Create(ctx, adapterCM)
		if err != nil {
			return ctrl.Result{}, err
		}

		logger.Info("STATUS: Adapter CM is created.")
		instance.Status.PodStatus.AdapterConfig = true

	}

	err = r.Status().Update(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	// END

	err = r.Status().Update(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	// CHECK RESOURCE STATUS AND REPORT

	pvcQuery := &corev1.PersistentVolumeClaim{}
	err = r.Get(ctx, pvcNamespacedName, pvcQuery)
	if err != nil && errors.IsNotFound(err) {
		logger.Info("RESOURCE: PVC is not created.")
		instance.Status.Storage.Ready = false
	} else if err != nil {
		logger.Info("RESOURCE: Cannot fetch PVC.")
		return ctrl.Result{}, err
	} else {
		logger.Info("RESOURCE: PVC is present.")
	}

	adapterCMQuery := &corev1.ConfigMap{}
	err = r.Get(ctx, adapterConfigMapNamespacedName, adapterCMQuery)
	if err != nil && errors.IsNotFound(err) {
		logger.Info("RESOURCE: Adapter CM is not created.")
		instance.Status.PodStatus.AdapterConfig = false
	} else if err != nil {
		logger.Info("RESOURCE: Cannot fetch Adapter CM.")
		return ctrl.Result{}, err
	} else {
		logger.Info("RESOURCE: Adapter CM is present")
	}

	podQuery := &corev1.Pod{}
	err = r.Get(ctx, podNamespacedName, podQuery)
	if err != nil && errors.IsNotFound(err) {
		logger.Info("RESOURCE: Pod is not created.")
		instance.Status.PodStatus.PodPhase = ""
	} else if err != nil {
		logger.Info("RESOURCE: Cannot fetch Pod.")
		return ctrl.Result{}, err
	} else {
		logger.Info("RESOURCE: PodPhase is " + string(podQuery.Status.Phase))
		instance.Status.PodStatus.PodPhase = string(podQuery.Status.Phase)
	}

	serviceQuery := &corev1.Service{}
	err = r.Get(ctx, serviceNamespacedName, serviceQuery)
	if err != nil && errors.IsNotFound(err) {
		logger.Info("RESOURCE: Service is not created.")
		instance.Status.ServiceConfig.IsAvailable = false
	} else if err != nil {
		logger.Info("RESOURCE: Cannot fetch service.")
		return ctrl.Result{}, err
	} else {
		logger.Info("RESOURCE: Service is present")
	}

	// END

	err = r.Status().Update(ctx, instance)
	if err != nil {
		return ctrl.Result{}, err
	}

	return ctrl.Result{}, nil
}

func (r *RobotReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&robotv1alpha1.Robot{}).
		Owns(&corev1.PersistentVolumeClaim{}).
		Owns(&corev1.ConfigMap{}).
		Owns(&corev1.Pod{}).
		Owns(&corev1.Service{}).
		Complete(r)
}
